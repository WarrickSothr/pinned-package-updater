FROM golang:1.17-buster as builder

ENV GO111MODULE=on

WORKDIR /go/src/app
COPY .. .

RUN go build -v

FROM ubuntu:20.04 AS server

WORKDIR /app

COPY --from=builder /go/src/app/pinned-package-updater .

ENTRYPOINT ["/app/pinned-package-updater"]
CMD ["serve"]