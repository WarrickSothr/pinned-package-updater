# Table Of Contents

1. [Logging](#logging)

## Logging

- Avoid print formatting messages on loggers, prefer to use fields on the logger to encapsulate additional information
    ```go
    // Avoid This Style, it bakes too much information into an unformatted message
    log.Debugf("something special %s happened", somethingSpecial)
  
    // Prefer this style with fields
    log.WithFields(log.Fields{
      "somethingSpecial": somethingSpecial
    }).Debug("something special happened")
    ```
- Pass loggers through the function chain as they become more specialized. This keeps extra fields on the loggers from
  higher contexts and preserves additional information captured during the process of getting to where the current
  line is being logged. The logger should be one of the first arguments defined for helper functions. Public functions
  should not require a logger to be passed, but may offer a similar method with a logger input.
    ```go
    func doSomething(logger *log.Entry, ...) {
      //does something
      logger.Debug("Something")
      specialLogger = logger.WithFields(log.Fields{
        "specialField", value
      })
      doSubSomething(specialLogger, ...)
    }
  
    func doSubSomething(logger *log.Entry, ...) {}
    ```