/*
Copyright © 2021 Drew Short <warrick@sothr.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package cmd

import (
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/thediveo/enumflag"
)

// LogFormat Custom enum for supported log output types https://pkg.go.dev/github.com/thediveo/enumflag#section-readme
type LogFormat enumflag.Flag

const (
	LoggingText LogFormat = iota
	LoggingLogFmt
	LoggingJson
)

var LogFormatIds = map[LogFormat][]string{
	LoggingText:   {"text"},
	LoggingLogFmt: {"logfmt"},
	LoggingJson:   {"json"},
}

var (
	configFile   string
	remoteURL    string
	logVerbosity int
	logFormat    LogFormat
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "pinned-package-updater",
	Short: "Update pinned packages in Dockerfiles",
	Long: `A longer description that spans multiple lines and likely contains
examples and usage of using your application. For example:

TODO: Add longer description with examples

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	// Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	cobra.CheckErr(rootCmd.Execute())
}

func init() {
	cobra.OnInitialize(initConfig, initLogging)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVar(&configFile, "config", "", "config file (default is $HOME/.pinned-package-updater.yaml OR ./.pinned-package-updater.yaml)")
	rootCmd.PersistentFlags().CountVarP(&logVerbosity, "verbose", "v", "verbose (-v, or -vv)")
	rootCmd.PersistentFlags().VarP(enumflag.New(&logFormat, "logFormat", LogFormatIds, enumflag.EnumCaseInsensitive), "format", "", "The logging format to use [logfmt, text, json]")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if configFile != "" {
		viper.SetConfigFile(configFile)
	} else {
		// Search config in home directory with name ".pinned-package-updater" (without extension).
		homeDirectory, homeDirectoryErr := os.UserHomeDir()
		cobra.CheckErr(homeDirectoryErr)

		viper.AddConfigPath(homeDirectory)
		viper.SetConfigType("yaml")
		viper.SetConfigName(".pinned-package-updater")

		// Search config in current directory with name ".pinned-package-updater" (without extension).
		currentDirectory, currentDirectoryErr := os.Getwd()
		cobra.CheckErr(currentDirectoryErr)

		viper.AddConfigPath(currentDirectory)
		viper.SetConfigType("yaml")
		viper.SetConfigName(".pinned-package-updater")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		_, _ = fmt.Fprintln(os.Stderr, "Using config file:", viper.ConfigFileUsed())
	}
}

func initLogging() {
	switch verbosity := logVerbosity; {
	case verbosity <= 0:
		log.SetLevel(log.ErrorLevel)
	case verbosity == 1:
		log.SetLevel(log.InfoLevel)
	case verbosity == 2:
		log.SetLevel(log.DebugLevel)
	case verbosity == 3:
		log.SetLevel(log.TraceLevel)
	case verbosity > 3:
		log.SetLevel(log.TraceLevel)
		// Add overhead to include the caller that sourced the error
		log.SetReportCaller(true)
	default:
		log.SetLevel(log.ErrorLevel)
	}

	switch logFormat {
	case LoggingText:
		log.SetFormatter(&log.TextFormatter{})
	case LoggingLogFmt:
		log.SetFormatter(&log.TextFormatter{
			DisableColors: true,
			FullTimestamp: true,
		})
	case LoggingJson:
		log.SetFormatter(&log.JSONFormatter{})
	default:
		log.SetFormatter(&log.TextFormatter{})
	}

	log.WithFields(log.Fields{
		"loggingFormatter": LogFormatIds[logFormat][0],
		"loggingLevel":     log.GetLevel().String(),
		"loggingVerbosity": logVerbosity,
	}).Info("configured logging")
}
