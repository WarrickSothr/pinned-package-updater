/*
Copyright © 2021 Drew Short <warrick@sothr.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package cmd

import (
	"os"
	"path"

	log "github.com/sirupsen/logrus"

	"github.com/spf13/cobra"

	"sothr.com/warricksothr/pinned-package-updater/internal/parser"
	"sothr.com/warricksothr/pinned-package-updater/internal/parser/dockerfile"
)

// checkCmd represents the check command
var checkCmd = &cobra.Command{
	Use:   "check",
	Short: "Check for updates to pinned packages in a alpine.Dockerfile",
	Long: `Read a alpine.Dockerfile, identify the repositories and packages it references, and locate required
updates to satisfy the requirement of running the latest security version for the pinned packages.

To run in standalone mode, provide a alpine.Dockerfile path to evaluate or rely on the default
behavior which looks for a alpine.Dockerfile in the current directory.

pinned-package-updater check [Dockerfile, ...]


To run in distributed mode, provide the remote address of the service that will
provide information about the remote.

pinned-package-updater --remote <address of the upstream service> check [Dockerfile, ...]
`,
	Run: func(cmd *cobra.Command, args []string) {

		// Handle no Dockerfile path provided
		if len(args) < 1 {
			currentDirectory, currentDirectoryErr := os.Getwd()
			cobra.CheckErr(currentDirectoryErr)
			defaultDockerfilePath := path.Join(currentDirectory, "Dockerfile")
			args = append(args, defaultDockerfilePath)
			log.WithFields(log.Fields{
				"defaultFilePath": defaultDockerfilePath,
			}).Debug("no dockerfile path provided, attempting to use the default dockerfile path")
		}

		// Aggregate the parsed updates
		numberOfFiles := len(args)
		for i, dockerfilePath := range args {
			log.WithFields(log.Fields{
				"filePath": dockerfilePath,
			}).Debugf("parsing docker file %d/%d", i+1, numberOfFiles)
			_, err := dockerfile.Parse(dockerfilePath)
			if err != nil {
				log.WithFields(log.Fields{
					"filePath":    dockerfilePath,
					"sourceError": err,
				}).Fatal("unexpected error while parsing dockerfile")
				return
			}
		}
	},
}

func init() {
	rootCmd.AddCommand(checkCmd)

	checkCmd.PersistentFlags().StringVar(&remoteURL, "remote", "", "remote url for distributed mode (example: https://ppu.example.com/)")

	// TODO: Add dynamic loading of maps between images and command parsers

	err := parser.InitCommandParsers()
	if err != nil {
		log.Fatal("unexpected error while initializing command parsers")
		return
	}

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// checkCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// checkCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
