# Pinned Package Updater

Pinned Package Updater (PPU) is a tool for managing pinned packages installed with package managers in a Dockerfile
manifest. The goal of this tool is to reduce the administrative burden of keeping packages up to date with upstream
security releases by offering an automatic mechanism for checking for new security releases and patching the existing
Dockerfile manifest with those new versions.

The integration into the CI/CD is left up to the user, but can be as simple as committing the changes and opening an MR.

## Table Of Contents

1. [Usage](#usage)
2. [Features](#features)
3. [Building](#building)
4. [Contributing](#contributing)
5. [License](#license)

## Usage

### Standalone Usage
The default behavior for the tool is to operate in a standalone mode. In this mode the tool is responsible for parsing
the Dockerfile, fetching the upstream package manager resources, identifying the upgrades available, and applying those
upgrades. An alternative [distributed mode](#distributed-usage) is available to remove the upstream package resource
caching and version upgrade determination logic.

```bash
pinned-package-updater check [Dockerfile, ...]
pinned-package-updater update [Dockerfile, ...]
```

### Distributed Usage

In distributed mode the tool relies on an external deployment of PPU that is running in ***serve*** mode 
`pinned-package-updater serve`. To cache the upstream package manager resources and handle the upgrade check logic. In
this mode much of the work can be cached between requests and the overhead of checking for pinned version updates is
significantly reduced.

The tool running in this mode will revert to standalone mode if the upstream service is unavailable.

```bash
pinned-package-updater check --remote <address of the upstream service> [Dockerfile, ...]
pinned-package-updater update [Dockerfile, ...]
```

## Features

- [ ] Supported base images
  - [ ] Alpine Linux 
    - (3.11, 3.12, 3.13, 3.14) are recognized by default, other versions may need additional configuration
  - [ ] Debian 
    - (jessie, stretch, buster, bullseye) are recognized by default, other versions may need additional configuration
  - [ ] Ubuntu 
    - (16.04, 18.04, 20.04) are recognized by default, other versions may need additional configuration
  - [ ] Red Hat Enterprise Linux 
    - (8, 9) are recognized by default, other versions may need additional configurations
  - [ ] Others with explicit configuration of package manager and default upstream repositories
- [ ] Support external repositories added in the image manifest
  - [ ] Support additional Alpine Linux repositories
  - [ ] Support additional Debian repositories
    - [ ] deb repositories
    - [ ] ppa repositories
  - [ ] Support additional Red Hat Linux repositories
- [ ] Resolve pinned packages in Dockerfiles
  - [ ] Support for Alpine based images (apk)
  - [ ] Support for Debian based images (apt, apt-get)
  - [ ] Support for Red Hat Enterprise Linux based images (yum, dnf)
- [ ] Interrogate upstream package systems for package and version information
  - [ ] Support for Alpine based package repositories
  - [ ] Support for Debian based package repositories
  - [ ] Support for Red Hat Enterprise Linux based package repositories
- [ ] Patch Dockerfiles in place with the recommended version upgrades

## Building

To build `pinned-package-updater`, run `go build`

## Contributing

I am not currently accepting outside contributions.

## License

```
Copyright © 2021 Drew Short <warrick@sothr.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```

A copy of the license can also be viewed at [LICENSE](LICENSE)