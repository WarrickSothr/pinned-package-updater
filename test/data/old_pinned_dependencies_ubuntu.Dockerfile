FROM ubuntu:18.04

RUN echo "deb http://archive.canonical.com/ubuntu bionic partner" > /etc/apt/sources.list.d/partner.list \
    && apt-get update && apt-get install -yq \
      ca-certificates=20210119~18.04.1 \
      curl=7.58.0-2ubuntu3.15 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \