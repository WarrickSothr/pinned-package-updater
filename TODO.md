# Outstanding TODO Items

- [ ] Add image parsing and baked in image lists
- [ ] Add override/supplemental configuration for images and mapped parsers
  - To support custom base images that aren't in the default list
- [ ] Add configuration override/supplemental configuration for repository lists
- [ ] Add runtime configuration option to specify a base image if one cannot be parsed ???
- [ ] Add apt repository parsing to lookup packages in upstream repositories
- [ ] Add version checking logic to identify the latest versions of available packages
- [ ] Design serialized output from `check` step for the `upgrade` step
  - `check` is responsible for parsing the docker image, determining available packages, and providing an output that is
    consumed by `upgrade` to apply the changes to the targeted Dockerfile(s).
- [ ] Implement `upgrade` step to apply suggested changes from `check` step
- [ ] Implement `serve` command to host a service that will offload the remote repository lookup and version suggestions